package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Dashboard;
import com.example.demo.entities.Order;
import com.example.demo.repository.OrderRepository;

@Service
public class OrderService {

	@Autowired
	private OrderRepository repository;

	public List<Order> getAllOrders() {
		return repository.getAllOrders();
	}

	public List<Dashboard> getDashboard() {
		return repository.getDashboard();
	}

	public String newOrder(Order order) {
		return repository.addOrder(order);
	}

	public Integer getBalance() {
		// TODO Auto-generated method stub
		return repository.getBalance();

	}

	public Integer getArray() {
		// TODO Auto-generated method stub
		return repository.getArray();
	}
}
