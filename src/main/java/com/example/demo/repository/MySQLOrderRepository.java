package com.example.demo.repository;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Dashboard;
import com.example.demo.entities.Order;

@Repository
public class MySQLOrderRepository implements OrderRepository {

	@Autowired
	JdbcTemplate template;

	int currentUser = 1;
	int totalNoOfShares = 0;
	double totalPrice = 0;

	@Override
	public List<Order> getAllOrders() {
		// TODO Auto-generated method stub
		String sql = "SELECT * from orders where userId=" + currentUser;
		System.out.println(template.query(sql, new OrderRowMapper()));
		return template.query(sql, new OrderRowMapper());
		// String sql = "SELECT OrderID, CompanyName, Phone FROM Orders";
		// return template.query(sql, new OrderRowMapper());
	}

	public Integer getBalance() {
		int balance = 0;
		String sql = "Select walletBalance from wallet where userId=" + currentUser;

		Map k = template.queryForMap(sql);
		balance = (int) k.get("walletBalance");

		return balance;
	}

	public List<Dashboard> getDashboard() {

		// TODO Auto-generated method stub
		String sql = "SELECT * from Dashboard where userId=" + currentUser;

		return template.query(sql, new dashMapper());
		// String sql = "SELECT OrderID, CompanyName, Phone FROM Orders";
		// return template.query(sql, new OrderRowMapper());
	}

	public Integer getArray() {

		String sql = "Select SUM(numOfShares) From Dashboard where userId=" + currentUser;

		Map k = template.queryForMap(sql);
		int temp1 = 0;
		temp1 = ((BigDecimal) k.get("SUM(numOfShares)")).intValue();

		return temp1;

	}

	@Override
	public String addOrder(Order order) {
		// TODO Auto-generated method stub
		double totalprice = 0;
		String sql1 = "";
		int sellVolume = 0;
		double change = 0.0;
		totalprice = order.getPrice() * order.getVolume();
		String sql2 = "SELECT walletBalance FROM Wallet WHERE userId=" + order.getUserId();

		int balance = 0;
		List<Integer> balanceList = template.query(sql2, new balanceMapper());
		for (Integer ent : balanceList) {
			balance = ent;
		}
		System.out.println(balance);

		Timestamp date = Timestamp.from(Instant.now());
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
		String formatted = format.format(date);
		// System.out.println(formatted);

		long TickerCount = 0;
		String sqlCheckTicker = "Select COUNT(*) from Dashboard where stockTicker='" + order.getStockTicker()
				+ "' AND userId=" + order.getUserId();
		// TickerCount=template.queryForObject(sqlCheckTicker, Integer);

		Map k = template.queryForMap(sqlCheckTicker);
		TickerCount = (long) k.get("Count(*)");

		// TODO Auto-generated method stub

		if (order.getBuyOrSell().equals("buy") && balance >= totalprice) {
			System.out.print("******");

			// try
			String sql = "INSERT INTO Orders(userId,stockTicker,price,volume,buyOrSell,statusCode,currentTime) "
					+ "VALUES(?,?,?,?,?,?,?)";
			template.update(sql, order.getUserId(), order.getStockTicker(), order.getPrice(), order.getVolume(),
					order.getBuyOrSell(), order.getStatusCode(), formatted);

			if (TickerCount == 0) {
				String sqlDash = "Insert into Dashboard(userId,stockTicker,numOfShares,avgPrice,currentPrice,profitOrLoss)"
						+ " values(?,?,?,?,?,?)";
				template.update(sqlDash, order.getUserId(), order.getStockTicker(), order.getVolume(), order.getPrice(),
						order.getPrice(), change);
			} else {
				String sqlDash = "Update Dashboard set numOfShares=numOfShares+" + order.getVolume() + ",currentPrice="
						+ order.getPrice() + " where stockTicker='" + order.getStockTicker() + "' AND userId='"
						+ order.getUserId() + "'";
				template.update(sqlDash);
			}

			sql1 = "UPDATE Wallet SET walletBalance=walletBalance-" + totalprice + " where userId=" + order.getUserId();
			template.execute(sql1);

			return "1";
		}

		/*
		 * List<Integer> dashList =template.query(sqlCheckTicker, new dashMapper()); for
		 * (Integer ent : dashList) { TickerCount=ent; }
		 */
		// System.out.println(TickerCount);
		// TickerCount=template.query(sql2, new dashMapper());

		else if (order.getBuyOrSell().equals("sell") && TickerCount == 1) {

			String sql3 = "SELECT numOfShares from Dashboard where stockTicker='" + order.getStockTicker()
					+ "' AND userId=" + order.getUserId();
			Map k1 = template.queryForMap(sql3);
			sellVolume = (int) k1.get("numOfShares");

			/*
			 * String sql3="SELECT SUM(volume) FROM Orders WHERE userId="+order.getUserId()
			 * +" AND stockTicker='"+order.getStockTicker()+"' AND buyOrSell=sell";
			 * List<Integer> sellVolumeList =template.query(sql3, new sellVolumeMapper());
			 * for (Integer ent : sellVolumeList) { sellVolume=ent; }
			 */

			if (order.getVolume() < sellVolume) {
				String sql = "INSERT INTO Orders(userId,stockTicker,price,volume,buyOrSell,statusCode,currentTime) "
						+ "VALUES(?,?,?,?,?,?,?)";
				template.update(sql, order.getUserId(), order.getStockTicker(), order.getPrice(), order.getVolume(),
						order.getBuyOrSell(), order.getStatusCode(), formatted);

				sql1 = "UPDATE Wallet SET walletBalance=walletBalance+" + totalprice + " where userId="
						+ order.getUserId();
				template.execute(sql1);

				String sqlDash = "Update Dashboard set numOfShares=numOfShares-" + order.getVolume() + ",currentPrice="
						+ order.getPrice() + " where stockTicker='" + order.getStockTicker() + "' AND userId='"
						+ order.getUserId() + "'";
				template.update(sqlDash);

				return "1";
			}

			else if (order.getVolume() == sellVolume) {
				String sql = "INSERT INTO Orders(userId,stockTicker,price,volume,buyOrSell,statusCode,currentTime) "
						+ "VALUES(?,?,?,?,?,?,?)";
				template.update(sql, order.getUserId(), order.getStockTicker(), order.getPrice(), order.getVolume(),
						order.getBuyOrSell(), order.getStatusCode(), formatted);

				sql1 = "UPDATE Wallet SET walletBalance=walletBalance+" + totalprice + " where userId="
						+ order.getUserId();
				template.execute(sql1);

				String sqlDash = "Delete from Dashboard where stockTicker='" + order.getStockTicker() + "' AND userId="
						+ order.getUserId();
				template.update(sqlDash);
				return "1";

			} else {
				return "0";
			}

		}

		else {
			return "0";
		}

	}

}

class OrderRowMapper implements RowMapper<Order> {

	@Override
	public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Order(rs.getInt("orderId"), rs.getInt("userId"), rs.getString("stockTicker"), rs.getDouble("price"),
				rs.getInt("volume"), rs.getString("buyOrSell"), rs.getInt("statusCode"), rs.getString("currentTime"));

	}

}

class balanceMapper implements RowMapper<Integer> {

	@Override
	public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		return rs.getInt("walletBalance");
	}
}

class sellVolumeMapper implements RowMapper<Integer> {

	@Override
	public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		return rs.getInt("volume");
	}

}

class dashMapper implements RowMapper<Dashboard> {

	@Override
	public Dashboard mapRow(ResultSet rs, int rowNum) throws SQLException {

		return new Dashboard(rs.getInt("userId"), rs.getString("stockTicker"), rs.getInt("numOfShares"),
				rs.getDouble("avgPrice"), rs.getDouble("currentPrice"), rs.getDouble("profitOrLoss"));

	}

}

/*
 * {"userId":1, "stockTicker":"CIPLA", "price":20.05, "volume":3,
 * "buyOrSell":"buy", "statusCode":1, "currentTime":"" }
 */