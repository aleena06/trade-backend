package com.example.demo.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.entities.Dashboard;
import com.example.demo.entities.Order;

@Component
public interface OrderRepository {
	public List<Order> getAllOrders();

	// public Order getOrderById(int id);

	// public Order editOrder(Order order);

	// public int deleteOrder(int id);

	public String addOrder(Order order);

	public List<Dashboard> getDashboard();

	public Integer getBalance();

	public Integer getArray();
}
