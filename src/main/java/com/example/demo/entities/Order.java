package com.example.demo.entities;

import java.sql.Timestamp;

public class Order {

	int orderId;
	int userId;
	String stockTicker;
	double price;
	int volume;
	String buyOrSell;
	int statusCode;
	String currentTime;
	// Timestamp.from(Instant.now())

	public Order() {

	}

	public Order(int orderId, int userId, String stockTicker, double price, int volume, String buyOrSell, int statusCode,
			String currentTime) {
		super();
		this.orderId = orderId;
		this.userId = userId;
		this.stockTicker = stockTicker;
		this.price = price;
		this.volume = volume;
		this.buyOrSell = buyOrSell;
		this.statusCode = statusCode;
		this.currentTime = currentTime;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setId(int id) {
		this.orderId = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserid(int userId) {
		this.userId = userId;
	}

	public String getStockTicker() {
		return stockTicker;
	}

	public void setStockTicker(String stockTicker) {
		this.stockTicker = stockTicker;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public String getBuyOrSell() {
		return buyOrSell;
	}

	public void setBuyOrSell(String buyOrSell) {
		this.buyOrSell = buyOrSell;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getCurrentTime() {
		return currentTime;
	}

	public void setCurrentTime(String currentTime) {
		this.currentTime = currentTime;
	}

}