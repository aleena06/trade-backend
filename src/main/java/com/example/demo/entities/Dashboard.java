package com.example.demo.entities;

public class Dashboard {

	int userId;
	String stockTicker;
	int numOfShares;
	double avgPrice;
	double currentPrice;
	double profitOrLoss;

	public Dashboard(int userId, String stockTicker, int numOfShares, double avgPrice, double currentPrice,
			double profitOrLoss) {
		super();
		this.userId = userId;
		this.stockTicker = stockTicker;
		this.numOfShares = numOfShares;
		this.avgPrice = avgPrice;
		this.currentPrice = currentPrice;
		this.profitOrLoss = profitOrLoss;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getStockTicker() {
		return stockTicker;
	}

	public void setStockTicker(String stockTicker) {
		this.stockTicker = stockTicker;
	}

	public int getNumOfShares() {
		return numOfShares;
	}

	public void setNumOfShares(int numOfShares) {
		this.numOfShares = numOfShares;
	}

	public double getAvgPrice() {
		return avgPrice;
	}

	public void setAvgPrice(double avgPrice) {
		this.avgPrice = avgPrice;
	}

	public double getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(double currentPrice) {
		this.currentPrice = currentPrice;
	}

	public double getProfitOrLoss() {
		return profitOrLoss;
	}

	public void setProfitOrLoss(double profitOrLoss) {
		this.profitOrLoss = profitOrLoss;
	}

}
