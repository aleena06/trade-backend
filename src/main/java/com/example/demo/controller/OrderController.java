package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Dashboard;
import com.example.demo.entities.Order;
import com.example.demo.service.OrderService;

@RestController
//@CrossOrigin("*")
@RequestMapping("orders")
public class OrderController {
	@Autowired
	OrderService service;

	@GetMapping(value = "/")
	public List<Order> getAllOrders() {
		// System.out.println("I am here");
		return service.getAllOrders();
	}

	@GetMapping(value = "/dashboard")
	public List<Dashboard> getDashboard() {

		return service.getDashboard();
	}

	@GetMapping(value = "/getTotalNumOfShares")
	public Integer getArray() {
		return service.getArray();

	}

	@PostMapping(value = "/")
	public String addOrder(@RequestBody Order order) {
		System.out.println(order.getStockTicker());
		System.out.println("######################");
		return service.newOrder(order);
	}

	@GetMapping(value = "/getBalance")
	public Integer getBalance() {
		return service.getBalance();
	}

}
